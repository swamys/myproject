import range from './range';

test('range', () => {
  expect(range(1, 1)).toEqual([]);
  expect(range(100, 100)).toEqual([]);
  expect(range(100, 1)).toEqual([]);
  expect(range(100, 101)).toEqual([100]);
  expect(range(1, 5)).toEqual([1, 2, 3, 4]);
});
