export const min2 = (x: number, y: number): number => {
  if (x < y) {
    return x;
  }
  return y;
};
