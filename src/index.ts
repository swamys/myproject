// // // console.log('hello world');

// // function min2(x: number, y: number): number {
// //   if (x < y) {
// //     return x;
// //   }
// //   return y;
// // }
// // console.log(min2(20, 30));

// // function max2(x: number, y: number): number {
// //   if (x > y) {
// //     return x;
// //   }
// //   return y;
// // }
// // console.log(max2(20, 30));

// // function max3(x: number, y: number, z: number): number {
// //   if (x > y) {
// //     return x;
// //   } else if (y > z) {
// //     return y;
// //   }
// //   return z;
// // }
// // console.log(max3(10, 12, 9));

// // function isFactor(n: number, i: number): boolean {
// //   if (n % i === 0) {
// //     return true;
// //   } else {
// //     return false;
// //   }
// // }
// // console.log(isFactor(7, 4));

// // function isEven(n: number): boolean {
// //   if (n % 2 === 0) {
// //     return true;
// //   } else {
// //     return false;
// //   }
// // }
// // console.log(isEven(6));

// // function isodd(n: number): boolean {
// //   if (n % 2 !== 0) {
// //     return true;
// //   } else {
// //     return false;
// //   }
// // }
// // console.log(isodd(7));

// // function isLeapyear(year: number): boolean {
// //   if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)) {
// //     return true;
// //   }
// //   return false;
// // }
// // console.log(isLeapyear(2021));

// // const alleven = (arr: number[]): number[] => {
// //   const b = [];
// //   for (let i = 0; i <= arr.length; ++i) {
// //     if (arr[i] % 2 === 0) {
// //       b.push(arr[i]);
// //     }
// //   }
// //   return b;
// // };
// // console.log(alleven([1, 2, 4, 6, 7, 8]));

// // function lies(a: number, x: number, y: number): boolean {
// //   if (x > a && a < y) {
// //     return true;
// //   }
// //   return false;
// // }
// // console.log(lies(2, 4, 6));

// // function indexofArr(arr: [0, 1], value: number): number {
// //   for (let i = 0; i < arr.length; i++) {
// //     if (arr[i] === value) {
// //       return i;
// //     }
// //   }
// //   return -1;
// // }
// // console.log(indexofArr([0, 1], 8));

// // function powr(x: number, y: number): number {
// //   let z = 1;
// //   for (let i = 1; i <= y; ++i) {
// //     z = z * x;
// //   }
// //   return z;
// // }
// // console.log(powr(4, 2));

// // function squareAll(arr: number[]): number[] {
// //   const arr2 = [];
// //   for (let i = 0; i < arr.length; ++i) {
// //     arr[i] = arr[i] * arr[i];
// //     arr2.push(arr[i]);
// //   }
// //   return arr2;
// // }
// // console.log(squareAll([1, 3, 9]));

// // function factorial(n: number): number {
// //   let fact = 1;
// //   for (let i = 1; i <= n; ++i) {
// //     fact = fact * i;
// //   }
// //   return fact;
// // }
// // console.log(factorial(5));

// // const arr = [1, 2, 3, 4, 5];
// // const [swamy, naveen, ...rest] = arr;
// // console.log(swamy, naveen, rest);

// // const sum = (arr1: number[], arr2: number[]): number[] => {
// //   const result: number[] = [];
// //   for (let i = 0; i < arr1.length; ++i) {
// //     result.push(arr1[i] + arr2[i]);
// //   }
// //   return result;
// // };
// // console.log(sum([5, 6, 9], [6, 8, 7]));

// // //concat
// // const concat = (arr1: number[], arr2: number[]): number[] => arr1.concat(arr2);
// // console.log(concat([5, 4, 3], [6, 7, 8]));

// // //range
// // const range = (start: number, stop: number): number[] => {
// //   const result: number[] = [];
// //   for (let i = start; i < stop; ++i) {
// //     result.push(i);
// //   }
// //   return result;
// // };
// // console.log(range(0, 15));

// // const factor = (n: number) => range(1, n).filter(i => n % i === 0);
// // console.log(factor(28));

// // //pop
// // const pop = (arr1: ReadonlyArray<number>) => {
// //   return arr1.slice(0, arr1.length - 1);
// // };
// // console.log(pop([1, 2, 3, 4]));

// // // const push = (arr2: ReadonlyArray<number>): number[] => {
// // //   return arr2.slice(9);
// // // };
// // // console.log(push.map(5));

// // const dropwhile = (
// //   arr: number[],
// //   f: (x: number) => boolean
// // ): ReadonlyArray<number> => {
// //   const drop = [];
// //   for (const e of arr) {
// //     if (f(e)) {
// //       arr.unshift(e);
// //     } else {
// //       drop.push(e);
// //     }
// //   }
// //   return drop;
// // };
// // console.log(dropwhile([2, 4, 6, 9, 12], x => x % 2 === 0));

const dropwhile = (
  arr: number[],
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const drop = [];
  for (const e of arr) {
    if (f(e)) {
      arr.unshift(e);
    } else {
      drop.push(e);
    }
  }
  return drop;
};
console.log(dropwhile([2, 4, 6, 9, 12], x => x % 2 === 0));
