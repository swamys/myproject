import factorial from './fact';

test('factorial', () => {
  expect(factorial(6)).toEqual([720]);
  expect(factorial(5)).toEqual([120]);
});
console.log(factorial(5));
