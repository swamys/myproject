const factorial = (n: number): number => {
  let fact = 1;
  for (let i = 1; i <= n; ++i) {
    fact = fact * i;
  }
  return fact;
};
export default factorial;
