const maxx = (x: number, y: number): number => (x > y ? x : y);

const maxarr2 = (arr4: ReadonlyArray<number>): number => arr4.reduce(maxx);
console.log(maxarr2([1, 7, 15, 99, 9, 125]));

const maxarr = (arr4: ReadonlyArray<number>): number =>
  arr4.reduce((x, y) => (x > y ? x : y));

console.log(maxarr([1, 7, 15, 99, 9, 125]));
