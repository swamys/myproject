const range6 = (start: number, stop: number): number[] => {
  const arr2 = [];
  for (let i = start; i <= stop; ++i) {
    arr2.push(i);
  }
  return arr2;
};
console.log(range6(1, 15));

const repeat1 = (x: number, n: number): ReadonlyArray<number> =>
  range(1, n).map(i => (i = x));
console.log(repeat1(2, 8));
