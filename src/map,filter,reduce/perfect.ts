const range3 = (start: number, stop: number): number[] => {
  const arr2 = [];
  for (let i = start; i <= stop; ++i) {
    arr2.push(i);
  }
  return arr2;
};
console.log(range3(1, 15));

const factors = (n: number): number[] => range(1, n).filter(i => n % i === 0);
console.log(factors(28));

const isPerfect = (n: number) =>
  factors(n).reduce((x, y) => x + y) === 2 * n ? true : false;
console.log(isPerfect(28));

const perfectNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => isPerfect(n));
console.log(perfectNumbers(1, 100));
