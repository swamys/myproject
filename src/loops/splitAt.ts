const splitAt = (
  arr: ReadonlyArray<number>,
  n: number
): [ReadonlyArray<number>, ReadonlyArray<number>] => {
  const one: number[] = [];
  for (let i = 0; i < n; i += 1) {
    one.push(arr[i]);
  }
  const second: number[] = [];
  for (let i = n; i < arr.length; i += 1) {
    second.push(arr[i]);
  }
  return [one, second];
};
console.log(splitAt([1, 2, 3], 4));
