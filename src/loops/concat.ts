const concat1 = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (const e of arr2) {
    arr1 = [...arr1, e];
  }
  return arr1;
};
console.log(concat1([1, 2, 3, 4, 5], [6, 7, 8, 9, 10]));
