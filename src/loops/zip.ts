const zip = (arr1: ReadonlyArray<number>, arr2: ReadonlyArray<number>) => {
  const z = [];
  for (let i = 0; i < arr1.length; i = i + 1) {
    z.push([arr1[i], arr2[i]]);
  }
  return [z];
};
console.log(zip([10, 20, 30], [40, 50, 60]));
