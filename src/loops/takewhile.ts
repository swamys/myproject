const takeWhile = (
  arr1: ReadonlyArray<number>,
  isEven1: (x: number) => boolean
): ReadonlyArray<number> => {
  const result = [];
  for (const e of arr1) {
    if (isEven1(e)) {
      result.push(e);
    } else {
      break;
    }
  }
  return result;
};

console.log(takeWhile([2, 4, 7, 12], x => x % 2 === 0));
