export const max3 = (x: number, y: number, z: number): number => {
  if (x > y) {
    if (x > z) {
      return x;
    } else {
      return z;
    }
  } else if (y > z) {
    return y;
  } else {
    return z;
  }
};
